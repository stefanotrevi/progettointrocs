\documentclass[12pt]{article}

%**********************************************
%* Add additional packages as needed

\usepackage{url,amsmath,setspace,amssymb}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{tcolorbox}
\usepackage{tikz}
\usepackage{caption}
\usepackage[noend]{algpseudocode}
\usepackage{algorithm}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{circuitikz}
\usepackage{paralist}
%**********************************************
%* Please replace this with your name and your AAU student number
\newcommand{\studentname}{Stefano Trevisani}
\newcommand{\studentnumber}{152018}


\hypersetup{colorlinks,allcolors=blue}
%**********************************************
%* Some more or less useful stuff, add custom stuff as needed

\lstnewenvironment{myalgorithm}[1][] %defines the algorithm listing environment
{
   % \captionsetup{labelformat=algocaption,labelsep=colon}
    \lstset{ %this is the stype
	mathescape=true,
	frame=none,
	numbers=none,
	basicstyle=\normalsize,
	keywordstyle=\color{black}\bfseries\em,
	keywords={input, output, return, datatype, function, in, if, else, foreach, while, begin,
end},
	numbers=left,
	xleftmargin=.04\textwidth,
	#1 % this is to add specific settings to an usage of this environment (for instance, the 
    % caption and referable label)
\/}
}
{}


\newtcolorbox{alert}[1]{
colback=red!5!white, colframe=red!75!white,fonttitle=\bfseries, title = #1}

\newtcolorbox{commentbox}[1]{
colback=black!5!white, colframe=black!75!white,fonttitle=\bfseries, title = #1}



%**********************************************
%* Leave the page configuration as is
\setlength{\oddsidemargin}{.25in}
\setlength{\evensidemargin}{.25in}
\setlength{\textwidth}{6.25in}
\setlength{\topmargin}{-0.4in}
\setlength{\textheight}{8.5in}

\newcommand{\unitname}{Introduction to Cybersecurity}
\newcommand{\maxpages}{5}
\newcommand{\handout}[3]{\heading{#1}{#2}{\studentname}{\studentnumber}{#3}}
\newcommand{\heading}[5]{
\renewcommand{\thepage}{#1-\arabic{page}}
\algrenewcommand{\algorithmicthen}{}
\algrenewcommand{\algorithmicdo}{}

\newcommand{\Tand}{\mathrel{\textbf{and}}}

\noindent
\begin{center}
	\framebox[\textwidth]{
	\begin{minipage}{0.9\textwidth} \onehalfspacing{}
	\textbf{622.755 -- \unitname} \hfill #2 % chktex 8

	{\centering \Large #5

	}\medskip
	{#3 \hfill #4}
	\end{minipage}
}
\end{center}
}

%**********************************************
%* The document starts here
\begin{document}
\handout{\maxpages}{Winter Term, 2020/21}{Project Write Up}
\begin{abstract}
	Secure multi-party computation (MPC) is a hot topic in modern cryptography; several
	protocols for computation without information leakege have been devised along the years,
	with Yao's \emph{garbled circuits protocol} (GCP) being one of the most prominent in the field.
	In this paper we will use GCP to solve the \emph{private set intersection} problem, 
	and we will discuss a possible application in healthcare.
\end{abstract}

\section{Overview}\label{sec:overview}
In the era of \emph{cloud computing}~\cite{Hayes2008}, the need of massive datacenters for the 
storage of data and powerful computers to process it is incredibly high.
Alas, the entites with the know-hows and the economic resources required to build and maintain such
facilities are very few: in fact, the overwhelming majority of the cloud storage offer in the
western countries for both individuals and companies is provided mostly by the likes of Amazon, 
Google and Microsoft; if we consider computing, the choices shrink down even more.
This a concern especially in the european countries: while EU projects like 
\href{https://eurohpc-ju.europa.eu}{EuroHPC} are starting to push for the computational independence, 
they are still at an early stage.

For these reasons, there is a compelling need of techiniques like homomorphic encryption schemes 
(HE)~\cite{AcarACU2018} and secure multi-party computation (MPC) protocols~\cite{Lindell2021}.
Even though very computationally expensive, they are the only sensible solution when computation 
has to be done by an untrusted third-party.

In Section~\ref{sec:yao} we will introduce one of the most famous MPC protocols: Yao's garbled
circuits, then in Section~\ref{sec:secure_set} we will show how garbled circuits can be used to
compute in a private way the intersection of two sets, and finally in Section~\ref{sec:applications}
we will show how private set intersection can be used in the contact tracing systems which are 
being used during the COVID-19 pandemic.

\section{Yao's garbled circuits}\label{sec:yao}
Yao's garbled circuits protocol~\cite{Yao1986} (GCP), provides a way to perform \emph{private}
computations between two parties, known as the \emph{garbler} and the \emph{evaluator}
(or Alice and Bob).
More formally, given Alice's inputs \(x\), Bob's inputs \(y\), and a boolean function of the kind 
\(z = f(x, y)\), Yao's GCP consists in building and evaluating a new `garbled' function  
\(G(z) = G(f)(G(x), G(y))\).

Note that the GCP ensures that the two parties cannot learn anything about each other's inputs 
\emph{by the evaluation of the circuit}: if the boolean function \(f\) is poorly designed, making
it easy to invert it when \(z\) is known together with one of the two inputs, then the garbling 
would be of no help.

Furthermore, the GCP is secure only for \emph{honest but curious} (or \emph{semi-honest})
parties~\cite{Lindell2009}: if one of the two parties is actively malicious, then the protocol is 
not secure anymore.
The GCP then involves the following macro-steps: 
\begin{enumerate}
	\item\label{step:build} Alice garbles the circuit, creating a set of \emph{keys} for her inputs,
	      for every possible input from Bob, and for the truth table of every gate; the rows of 
	      the truth tables are permuted, then she sends the garbled circuit, her input keys, and the
	      output keys to Bob.
	\item Bob now engages in an \emph{oblivious transfer} protocol with Alice to get the
	      correct keys for his inputs, because if Bob knew both the keys, he would be able to decrypt 
	      the circuit, and if Alice knew the choice of Bob, she would know his inputs.
	\item Bob now has to evaluate the circuit: this can be done by employing an authenticated
	      encryption scheme or a \emph{point-and-permute} vector.
	      In the first case, Bob finds the right row of the truth table because decrypting all the 
	      other ones fails.
	      In the second case, during Step~\ref{step:build}, Alice XORes every encrypted input bit and 
	      intermediate key with a permutation bit, so they always point to the right row.
	\item Finally, Bob sends the decrypted output bits, the only ones which are not keys but actual
	      values, to Alice.
\end{enumerate}
There are several different oblivious transfer (OT) protocols, one of the most famous and easy to 
understand is certainly the Bellare-Micali scheme~\cite{BellareM1989}.
\section{Private Set Intersection}\label{sec:secure_set}
Many basic problems that can benefit from MPC schemes like the GCP are operations on sets.
In this section we will se how to implement private set intersection (PSI) using Yao's GCP~\cite{HuangEK2012}.
We will refer to the sets of Alice and Bob as \(A\) and \(B\) respectively.

\subsection{The Big-AND Algorithm}
Assume that \(A\) and \(B\) draw elements from some finite universe \(U\) with a total order \(<\); 
any subset of \(U\) can be represented as a vector of \(|U|\) bits where a bit is 1 (resp.\ 0) 
if the corresponding element is present (resp.\ absent).
So, if we consider such arrays as unsigned integers, \(A \cap B = \texttt{A \& B}\), so our circuit 
would be made of exactly \(|U|\) AND gates outputting the resulting intersection in output, as 
shown in Figure~\ref{fig:circuits}.
\begin{figure}[t]
	\centering
	\begin{circuitikz}[scale=0.75]
		\draw{}
		(0, 0)   node[and port,rotate=-90] (and1) {}
		(2, 0)   node[and port,rotate=-90] (and2) {}
		(6, 0)   node[and port,rotate=-90] (and3) {}
		(8, 0)  node[and port,rotate=-90] (and4) {}
		
		(and1.in 1) node [anchor=south] {\(b_1\)}
		(and1.in 2) node [anchor=south] {\(a_1\)}
		(and2.in 1) node [anchor=south] {\(b_2\)}
		(and2.in 2) node [anchor=south] {\(a_2\)}
		(and3.in 1) node [anchor=south] {\(b_m\)}
		(and3.in 2) node [anchor=south] {\(a_m\)}
		(and4.in 1) node [anchor=south] {\(b_n\)}
		(and4.in 2) node [anchor=south] {\(a_n\)}
		(and1.out)  node [anchor=north] {\(o_1\)}
		(and2.out)  node [anchor=north] {\(o_2\)}
		(and3.out)  node [anchor=north] {\(o_m\)}
		(and4.out)  node [anchor=north] {\(o_n\)};
		\draw[loosely dotted,line width=2pt](3, 0.5) -- (5, 0.5);
	\end{circuitikz}
	\begin{circuitikz}[scale=0.75]
		\draw{}
		(0, 0)  node[xor port,rotate=-90] (xor1) {}
		(2, 0)  node[xor port,rotate=-90] (xor2) {}
		(6, 0)  node[xor port,rotate=-90] (xor3) {}
		(8, 0)  node[xor port,rotate=-90] (xor4) {}
		(1, -2) node[or port,rotate=-90] (or1) {}
		(7, -2) node[or port,rotate=-90] (or2) {}
		(4, -4) node[or port,rotate=-90] (or3) {}
		
		(xor1.in 1) node [anchor=south] {\(b_1\)}
		(xor1.in 2) node [anchor=south] {\(a_1\)}
		(xor2.in 1) node [anchor=south] {\(b_2\)}
		(xor2.in 2) node [anchor=south] {\(a_2\)}
		(xor3.in 1) node [anchor=south] {\(b_m\)}
		(xor3.in 2) node [anchor=south] {\(a_m\)}
		(xor4.in 1) node [anchor=south] {\(b_n\)}
		(xor4.in 2) node [anchor=south] {\(a_n\)}
		(or3.out) node [anchor=north] {\(o_1\)}
		
		(xor1.out) -- (or1.in 2)
		(xor2.out) -- (or1.in 1)
		(xor3.out) -- (or2.in 2)
		(xor4.out) -- (or2.in 1);
		\draw[loosely dotted,line width=1pt]
		(or1.out) -- (or3.in 2)
		(or2.out) -- (or3.in 1);
		\draw[loosely dotted,line width=2pt](3, .5) -- (5, .5);
	\end{circuitikz}
	\caption{Left: the Big-AND circuit. Right: the circuit for the naive algorithm.}\label{fig:circuits}
\end{figure}

While very simple, this algorithm has two problems: 
\begin{inparaenum}[(i)]
	\item If one party, say Alice, uses a vector of all 1, then the intersection will result 
	in Bob's set, and he would have no way to detect it.
	\item The circuit grows linearly with \(|U|\) (\(\sim\) exponentially with the size of its 
	elements).
	
	\subsection{The Naive algorithm}
	Assuming that the elements of \(U\) can be represented as \(\log(|U|)\) bit integers, the naive 
	algorithm uses an equality comparison circuit between as its basic operation, and by applying
	it to every pair in \(A \times B\), it finds the intersection. 
	The equality comparison circuit, as can be seen in Figure~\ref{fig:circuits}, is simply a bitwise XOR 
	between the two inputs, followed by an OR-chain that reduces the output to a single bit, which is 
	0 when the two inputs are equal and 1 otherwise\footnote{if we do not reduce the output, Bob could
		just XOR it with its input and retrieve Alice's input.}.
\end{inparaenum}

With this algorithm both parties can detect if the other one is using a `strange' set (Alice can 
detect it during the OT phase, Bob while he is receiving Alice's inputs), and the circuit remains 
very small as it grows linearly with \(\log(|U|)\).
The main drawback of this algorithm is that it requires \(\Theta(|A||B|)\) circuit evaluations, 
so it's not very efficient.

\subsection{The Sorted Intersection Algorithm}
If the elements in \(A\) and \(B\) are sorted, we can use Algorithm~\ref{alg:fast} to find the 
intersection.

\begin{algorithm}
	\begin{algorithmic}[1]
		\Function{sortSec}{A, B}
		\State{\(C, i, j \gets \emptyset, 0, 0 \)}
		\While{\(i < |A| \Tand j < |B|\)}
		\If{\(A[i] = B[j]\)}
		\State{\(C, i, j \gets C \cup \{B[j]\}, i + 1, j + 1 \)}
		\ElsIf{\(A[i] < B[j]\)}
		\State{\(i \gets i + 1\)}
		\Else{}
		\State{\(j \gets j + 1\)}
		\EndIf{}
		\EndWhile{}
		\State{\Return{\(C\)}}
		\EndFunction{}
	\end{algorithmic}
	\caption{The sorted intersection algorithm.}\label{alg:fast}
\end{algorithm}
This algorithm uses a comparator circuit which outputs 00 when \(a = b\), 01 when \(a > b\), and 11 
otherwise, defined as follows:
\begin{align*}
	 & o^0_1 = 0                            &  & o^0_2 = 0                                \\
	 & o^i_1 = o^{i-1}_1 + (a_i \oplus b_i) &  & o^i_2 = (o^{i-1}_2 \times (o^{i-1}_1)) +
	(b_i \times -o^{i-1}_1)
\end{align*}
One can immediatly see how the sorted intersection algorithm requires only \(\Theta(|A| + |B|)\)
evaluations, making it much faster.
The main issue here is that Bob can know the intervals where the elements of Alice lie: this becomes 
less relevant if the elements are actually hashes.

\section{Applying PSI}\label{sec:applications}
There are many real world cases where PSI is used to ensure that there is no leakege of 
information, for example see~\cite{BrickellPSW2007,DemmlerRRT2018,HEMFS2017,IonKNPSSSY2017}.
In the period of the COVID-19 pandemic, many countries have implemented some kind of 
\emph{contact tracing system} (CTS), that tracks the movements of the people who install the 
companion smartphone application.
CTSes have been the topic of intense pubblic debate, especially for concerns about the 
privacy of the users.
Clearly, any system that stores and elaborates personal and/or sensitive data (as by the GDPR), 
like geographical location and health condition, of a large number of people, must be designed with 
privacy as a primary focus.

For example, suppose that a CT application stores, for privacy reasons, the set of locations where 
the user has been only on his phone.
Now suppose that Alice is diagnosed with COVID\@: it would certainly be of interest finding out
who could have been in contact with her in the last few days. 
To do so, we need to compute the intersection of the location set of every user with the location 
set of Alice (there are many ways to reduce the actual amount of users involved); if we carry out
this computation using PSI, we avoid to reveal the locations where all the other people have been, 
and if Alice was alone in some place, that is not revealed either.
Note that set intersection reveals a lot of information, like \emph{how many} and \emph{which}
places are in common by two people: a simple hash is enough to avoid revealing the \emph{which}
part. 
The amount of common places can actually be useful to rank the contacts with a priority that can 
be used to optimize the work of the medical staff and immediatly reach people with the higher risk
of being infected.

\section{Conclusion}
In this paper we gave a quick overview of Yao's GCP and how it could be applied to compute the 
intersection of two sets in a private way, showing how this can be employed in a very real scenario
like privacy-preserving contact tracing during a pandemic. 
We believe that, in a world where the privacy of people is always endangered, a lot of sensitive 
personal data is on sale, and laws often struggle to protect the citizens, studying and developing 
HE/MPC systems to make them more efficient and more widely used can have a positive impact.

\bibliographystyle{plain}
\bibliography{biblio}

\end{document}
