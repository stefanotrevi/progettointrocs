\section{Execution Flow}

\subsection{Preceding the MPC computation}
\subsubsection{Parameters setting}
The script starts by instantiating an object \texttt{LocalTest}, defining it with the arguments given at launch.
Our \texttt{LocalTest} class is a modified version of the one from the Yao library and represents the core of
our MPC implementation.

\subsubsection{Input reading and preprocessing}
From the constructor, the function \texttt{local\_steps} is called in order to read the input data from the files (or randomly generate it if
asked at launch) and preprocess it. These preprocessing activities remove duplicates found in the input sets and sort
them. Then the input elements are converted from integer to floating point, in order to perform our conversion to
fixed point decimals as required by the assignment.\\
\texttt{local\_steps} also calls \texttt{init\_derived\_params} in order to initialize the parameters that depend
on the number of bits of the circuit. These parameters will then be used for the fixed point conversion and the
circuit construction.

\subsubsection{Fixed point conversion}
The float to fixed point conversion is performed inside the \texttt{convert} function for each element in Alice's and Bob's
input sets. For each element, the function \texttt{to\_fixed} is called to compute the actual conversion on each element.
To do this, \texttt{to\_fixed} takes in input a float and multiplies it with the previously computed parameter \texttt{DEC\_ROUND}
to shift all the decimal digits into the integer part.\\
This number is then rounded, in order to get rid of any other decimal digits
which could not be represented with the chosen number of bits for the current execution.\\
Finally, a bitwise
\texttt{AND} operation of this value with the parameter \texttt{MAX\_INT} is performed. \texttt{MAX\_INT} is the largest representable
number with the number of bits used in the current execution. Being this number's binary representation a sequence of \texttt{1}s,
the bitwise \texttt{AND} operation will set the binary representation of the converted value to the fixed number of bits, without
affecting its value.\\
This procedure's pseudocode is illustrated in Algorithm~\ref{alg:tofixed}.

\begin{algorithm}
	\begin{algorithmic}
		\Procedure{to\_fixed}{$x$}
			\State{$shifted\gets x*\texttt{DEC\_ROUND}$}
			\State{\textbf{return} \Call{Round}{$shifted*\texttt{MAX\_INT}$}}\Comment{$*$ stands for bitwise \texttt{AND}}
		\EndProcedure{}
	\end{algorithmic}
	\caption{From Floating Point to Fixed Point}\label{alg:tofixed}
\end{algorithm}

The opposite conversion is performed once Bob has terminated the evaluation of the circuits. For each result the function
\texttt{from\_fixed} is called. This function takes an integer as an input and gets the float conversion dividing it by \texttt{DEC\_ROUND},
rounding the result to the number of decimal digits calculated during the initialization of the conversion parameters.
This procedure's pseudocode is illustrated in Algorithm~\ref{alg:fromfixed}.

\begin{algorithm}
	\begin{algorithmic}
		\Procedure{from\_fixed}{$x$}
			\State{$shifted\gets x/\texttt{DEC\_ROUND}$}
			\State{\textbf{return} \Call{Round}{$shifted, \texttt{DEC\_DIGITS}$}}
		\EndProcedure{}
	\end{algorithmic}
	\caption{From Fixed Point to Floating Point}\label{alg:fromfixed}
\end{algorithm}

\subsubsection{Circuit generation}
Finally, the constructor builds the circuit for the MPC computation based on the number of bits given at launch.\\
The constructor will call either the function \texttt{generate\_equal\_circuit} or \texttt{generate\_compare\_circuit}
based on the computing algorithm chosen by the user (\texttt{slow}/\texttt{fast}). This phase of the computation is
more deeply illustrated in Section~\ref{sec:circuit}

\subsection{Starting the Yao protocol}

\subsubsection{Circuit garbling}
The function \texttt{alice\_to\_bob} formally starts the Yao protocol. Here, Alice garbles the circuit.
The garbling is performed entirely through the functions offered by the Yao library. The garbled circuit is then
sent to Bob, together with Alice's encrypted inputs.

\subsubsection{Local Oblivious Transfer}
The \texttt{LocalObliviousTransfer} class is defined inside \texttt{localOT.py} and its purpose is to simulate a
1-out-of-2 OT between Alice and Bob in a local environment.
It achieves this by printing to \texttt{stdout} the main communications between the two parties, thus having the
console simulating the behavior of the socket. We explicitly chose to print only the most relevant exchanges
between Alice and Bob in order to have a more readable, organized output.\\
This process starts with a call to the \texttt{alice\_bob\_ot} function.
Inside the function, Bob maps each of its input bits to his circuit wires. Then, for each element in his set,
Bob requests a sequence of \(n\) OTs, where \(n\) is the number of Bob's wires in the circuit.
Although there is no actual change of context, in the function \texttt{MPC\_Computation} we are assuming that
Bob is calling the function \texttt{start\_ot} from the class \texttt{LocalObliviousTransfer} in order to start
the OT on a specific wire. The function \texttt{start\_ot} will then proceed to perform the actual OT and
print on the console all the most pertinent exchanges between Alice and Bob.

\begin{algorithm}
	\begin{algorithmic}
		\For{\(element \in BobSet\)}
		\For{\(wire \in BobWires\)}
		\State \Call{start\_ot}{wire}
		\EndFor{}
		\EndFor{}
	\end{algorithmic}
	\caption{Bob starts OT}\label{alg:bobstartOT}
\end{algorithm}

Both Alice's and Bob's computations for the OT are done inside \texttt{start\_ot}. We used comments to define whether a specific
computation is done by either Alice or Bob.\\
Entering in \texttt{start\_ot}, the request of OT by Bob is printed. Upon receiving the request, Alice retrieves the two keys for
the wire specified by Bob and sends them to him. This pair of keys is also printed on console.\\
The function then proceeds performing the OT in the same way as the Yao library, which follows the construction illustrated in~\cite{smart2015crypto}
Bellare-Micali construction~\cite{bellaremicali1989}.
Algorithm~\ref{alg:ot} provides the pseudocode of the construction.\\
During this whole process, the bit \(b\) chosen by Bob is also printed on console.
\begin{algorithm}
	\begin{algorithmic}
		\Require{Cyclic abelian group \(G\) of prime order \(p\) with generator \(g\)}
		\Require{Hash function \(H: G\xrightarrow[]{}{\{0,1\}}^n\)}
		\Require{Bit \(b\xleftarrow[]{}\{0,1\}\) chosen by Bob}
		\State{Alice chooses \(c\xleftarrow[]{\text{\$}}G\)}
		\State \Call{SendToBob}{$c$}
		\State{Bob chooses \(x\xleftarrow[]{\text{}}Z_p\)}
		\State{\(h_b\xleftarrow{}g^x\)}
		\State{\(h_{b-1}\xleftarrow{}\frac{c}{g^x}\)}
		\State \Call{SendToAlice}{$h_0, h_1$}
		\State{Alice chooses \(k\xleftarrow[]{\text{}}Z_p\)}
		\State{\(c_1\xleftarrow[]{\text{}}g^k\)}
		\State{\(e_0\xleftarrow[]{}H(h_0^{k})\oplus{m_0}\)}
		\State{\(e_1\xleftarrow[]{}H(h_1^{k})\oplus{m_1}\)}
		\State \Call{SendToBob}{$c_1, e_0, e_1$}
		\State{\(m_b\xleftarrow[]{}e_b\oplus{{H(c_1)}^x}\)}
	\end{algorithmic}
	\caption{Oblivious Transfer}\label{alg:ot}
\end{algorithm}

\subsubsection{Circuit evaluation}
Lastly, the function \texttt{bob\_mpc\_compute} is called in order to start Bob's evaluation of the circuit.
The evaluation is done using the functions offered by the Yao library, which implements a point-and-permute
optimization that allows Bob to identify the correct row of each garbled table in one single guess. This kind
of evaluation slightly differs from the one explained during the lectures since Bob is directly finding the
circuit's output, instead of finding its corresponding key.\\
In our implementation, we are assuming that Bob is sharing the results with Alice only after evaluating the
circuit of each comparison. Here we also provide insights about the time of the execution, in order to
estimate how fast the whole computation is based on different inputs and settings.\\
Right after, the function \texttt{alice\_mpc\_compute} is called, which serves solely as a placeholder to
simulate Alice's receiving of the results.\\
In the end, \texttt{verify\_output} is called in order to check the correctness of the MPC computation by
comparing it with the result of the intersection of the two sets computed with the \texttt{intersection}
function for Set objects.

