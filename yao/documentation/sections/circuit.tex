\section{Description of the circuit}\label{sec:circuit}
As we said, there are two different algorithms available in the script, and each one uses two
different circuits: the \emph{equality} circuit and the \emph{comparator} circuit.
Both circuits are not hardcoded, but are dynamically generated for an arbitrary number of bits,
with the only constraint that it has to be a power of 2 (this constraint is actually needed only
for the equality circuit).

The equality circuit takes in input 2 \(n\)-bit numbers \(a, b\) and outputs 0 if the two numbers
are equal, otherwise it outputs 1.
To do so, we apply a XOR gate to every pair of inputs \(a_i, b_i\), obtaining
\(n\) bits which are all 0 if and only if the two numbers were equal.
Clearly, we cannot use this as an output, since it would reveal to Bob the value of \(a\) as soon
as he tries to XOR the result with \(b\).
So, we apply a \emph{tree} of OR gates that performs a reduction on the output bits: all the even
output bits are ORed with the odd output bits until we end with only one bit in output (this is
why we need \(n\) to be a power of 2)\footnote{It is of course possible to modify the circuit to
  work with any number of bits: while in a hardware implementation it would likely be a heresy,
  in our software evaluator it would make no difference.
  Yet, we prefer to stick with the cleaner circuit.}.
Figure~\ref{fig:eq_4bit} shows the circuit that is generated for 4-bit inputs.
\begin{figure}
  \centering
  \begin{circuitikz}
    \draw{}
    (0, 0)  node[xor port,rotate=-90] (xor1) {}
    (2, 0)  node[xor port,rotate=-90] (xor2) {}
    (4, 0)  node[xor port,rotate=-90] (xor3) {}
    (6, 0)  node[xor port,rotate=-90] (xor4) {}
    (1, -2) node[or port,rotate=-90] (or1) {}
    (5, -2) node[or port,rotate=-90] (or2) {}
    (3, -4) node[or port,rotate=-90] (or3) {}

    (xor1.in 1) node [anchor=west] {\(b_1\)}
    (xor1.in 2) node [anchor=east] {\(a_1\)}
    (xor2.in 1) node [anchor=west] {\(b_2\)}
    (xor2.in 2) node [anchor=east] {\(a_2\)}
    (xor3.in 1) node [anchor=west] {\(b_3\)}
    (xor3.in 2) node [anchor=east] {\(a_3\)}
    (xor4.in 1) node [anchor=west] {\(b_4\)}
    (xor4.in 2) node [anchor=east] {\(a_4\)}
    (or3.out) node [anchor=east] {\(o_1\)}

    (xor1.out) -- (or1.in 2)
    (xor2.out) -- (or1.in 1)
    (xor3.out) -- (or2.in 2)
    (xor4.out) -- (or2.in 1)
    (or1.out) -- (or3.in 2)
    (or2.out) -- (or3.in 1);
  \end{circuitikz}
  \caption{The equality circuit that is generated for 4-bit inputs.}\label{fig:eq_4bit}
\end{figure}

The comparator circuit is a little more complex.
It again takes in input 2 \(n\)-bit numbers \(a, b\), but it has 2 output bits: the first bit
is 0 \(a = b\), otherwise it is 1.
The second bit is 0 if \(a \ge b\), otherwise it is 1.
This is needed for the fast algorithm since it must know which is the bigger number to work.
At a certain step \(i\), the comparator applies a XOR gate to the bits \(a_i\) and \(b_i\) to
check equality, and ORs the result with the temporary equality output bit of the previous step,
updating it: at the last step, this will be the first output bit.
In the meanwhile, we use the temporary equality value to choose (with a \emph{multiplexer})
between the old value of the second output bit and \(b_i\).
More formally:
\begin{align*}
   & o^0_1 = 0;\;\; o^0_2 = 0                                         \\
   & o^i_1 = o^{i-1}_1 + (a_i \oplus b_i)                             \\
   & o^i_2 = (o^{i-1}_2 \times (o^{i-1}_1)) + (b_i \times -o^{i-1}_1) \\
\end{align*}

Figure~\ref{fig:cmp_4bit} shows the circuit that is generated for 4-bit inputs (the actual circuit 
is a bit different: to simplify the generation, the script creates some `placeholder' gates).
\begin{figure}
  \centering
  \includegraphics[scale=0.25]{photo_2021-02-01_22-43-44.jpg}
  \caption{The comparator circuit generated for 4-bit inputs.}\label{fig:cmp_4bit}
\end{figure}
