#!/usr/bin/env python3
import logging
import localOT
import util
import yao
import json
import pickle
from random import randint, random
from math import log10, log2
from abc import ABC, abstractmethod
from time import perf_counter_ns as chrono

logging.basicConfig(format="[%(levelname)s] %(message)s",
                    level=logging.WARNING)


class YaoGarbler(ABC):
    """An abstract class for Yao garblers (e.g. Alice)."""

    def __init__(self, circuits):
        circuits = util.parse_json(circuits)
        self.name = circuits["name"]
        self.circuits = []

        for circuit in circuits["circuits"]:
            garbled_circuit = yao.GarbledCircuit(circuit)
            pbits = garbled_circuit.get_pbits()
            entry = {
                "circuit": circuit,
                "garbled_circuit": garbled_circuit,
                "garbled_tables": garbled_circuit.get_garbled_tables(),
                "keys": garbled_circuit.get_keys(),
                "pbits": pbits,
                "pbits_out": {w: pbits[w]
                              for w in circuit["out"]},
            }
            self.circuits.append(entry)

    @abstractmethod
    def start(self):
        pass


def bitset(x: int, bitsz: int) -> list:
    """ Convert an int to an array of bits """
    return [(x >> i) & 1 for i in range(bitsz - 1, -1, -1)]


def round2(x: int) -> int:
    """ Round a number to the nearest power of 2"""
    return 1 << (x - 1).bit_length() if x else 0


def bits_from_float(data: list) -> int:
    """ Deduce the bits to represent any float as a fixed (up to a truncation)"""
    sdata = [str(d).split('.') for d in data]
    sdata_i = [d[0] for d in sdata]
    sdata_d = [d[1] if len(d) > 1 else '0' for d in sdata]
    max_i = len(max(sdata_i, key=len))
    max_d = len(max(sdata_d, key=len))
    bit_i = int(max_i * log2(10)) + 1
    bit_d = int(max_d * log2(10)) + 1

    return bit_i, bit_d


def sum_frac2(n: int) -> float:
    """ Sum the reciprocals of the first n powers of 2"""
    k = .5
    res = 0
    for _ in range(n):
        res += k
        k /= 2

    return res


class LocalTest(YaoGarbler):
    """A class for local tests. It can either work on given inputs or on randomly generated inputs.

    Args:
        bits: The maximum size in bits of the input data

        numel: Number of elements randomly generated, if <= 0 reads alice.json and bob.json as input

        algo: Defines the algorithm being used for the computation of the common elements.
        If not defined, it is set to "slow" which is the naive algorithm.

        type: The type of the elements randomly generated.

        ot: Defines whether a local version of Oblivous Transfer is used (true) or not (false).
    """
    CIRCUIT_DIR = "circuits"
    CIRCUIT_FNAME = "common"
    CIRCUIT_EXT = ".json"
    CIRCUIT_FILE = CIRCUIT_DIR + "/" + CIRCUIT_FNAME + CIRCUIT_EXT

    DB_DIR = "db"
    DB_EXT = ".json"
    ALICE_FNAME = "alice"
    ALICE_FILE = DB_DIR + "/" + ALICE_FNAME + DB_EXT
    BOB_FNAME = "bob"
    BOB_FILE = DB_DIR + "/" + BOB_FNAME + DB_EXT

    def __init__(self, bits: int, numel: int, algo: str, type: str, ot: bool):
        # parameters for integer calculations
        self.NUMEL = numel  # number of elements in Alice and Bob sets for RNG
        self.BITS = round2(bits if self.NUMEL <= 0 else max(bits, 4))  # number of bits
        self.INT_BITS = self.BITS >> 1  # bits for the integral part
        self.DEC_BITS = self.INT_BITS  # bits for the decimal part
        self.ENABLE_OT = ot
        self.ALG_SLOW = (algo == "slow")
        self.IS_FLOAT = (type == "float")
        # Initialize the OT protocol
        self.local_ot = localOT.LocalObliviousTransfer()
        self.init_derived_params()

        # Alice and Bob initialize their data
        self.a_data, self.b_data = self.local_steps()

        # Generate the plain circuit
        if self.ALG_SLOW:
            self.generate_equal_circuit()
        else:
            self.generate_compare_circuit()

        # Garble the circuit
        super().__init__(self.CIRCUIT_FILE)

    def init_derived_params(self):
        """ Initialize all the parameters that depend on the number of bits"""

        # parametes for the integer calculations
        self.MAX = (1 << self.BITS) - 1

        # parameters for fixed/floating point calculation and conversion
        self.INT_MAX = (1 << self.INT_BITS) - 1
        self.DEC_MAX = sum_frac2(self.DEC_BITS)
        self.REAL_MAX = self.INT_MAX + self.DEC_MAX
        self.DEC_ROUND = 1 << self.DEC_BITS  # value to remove the decimal point
        self.DEC_DIGITS = int(self.DEC_BITS * log10(2)) + 1  # number of digits of the decimal part

    def start(self):
        """Local Yao protocol."""

        # Alice garbles the circuit and sends it to Bob, together with Alice's input keys
        circuit, a_inputs = self.alice_to_bob(self.a_data[1])

        # Bob gets its input keys via OT
        b_inputs = self.alice_bob_ot(circuit, self.b_data[1])

        # Bob evaluates the circuit
        match_yao = self.bob_mpc_compute(circuit, a_inputs, b_inputs, self.b_data[1])

        # Alice receives the results (placeholder)
        self.alice_mpc_compute()

        # Verify that the output is correct
        self.verify_output(self.a_data, self.b_data, match_yao)

        print(f"Thank you for choosing our MPC suite, goodbye!")

    def local_steps(self):
        """ Performs local steps that Alice and Bob do before starting the communication"""

        # Print the original data
        print("======== 0a. Alice's and Bob's original data ========")
        a_data = self.load_data(self.ALICE_FILE)
        b_data = self.load_data(self.BOB_FILE)
        print(f"Alice: {a_data}")
        print(f"Bob: {b_data}")
        print()

        # Deduce properties (or use given ones)
        print("======== 0b. Alice's and Bob's data properties ========")
        self.IS_FLOAT = self.IS_FLOAT or\
            any(isinstance(x, float) for x in a_data) or any(isinstance(x, float) for x in b_data)

        if not self.BITS:  # automatically deduce required bit size
            if not self.IS_FLOAT:
                self.BITS = round2(int(max(a_data + b_data)).bit_length())
                self.INT_BITS = self.BITS >> 1
                self.DEC_BITS = self.INT_BITS
            else:
                self.INT_BITS, bit_d = bits_from_float(a_data + b_data)
                self.BITS = round2(self.INT_BITS + bit_d)
                self.DEC_BITS = self.BITS - self.INT_BITS
            self.init_derived_params()

        print(f"Bit length: {self.BITS}")
        if self.IS_FLOAT:
            print("Data type: float")
            print(f"Maximum value representable: {self.REAL_MAX}")
        else:
            print("Data type: int")
            print(f"Maximum value representable: {self.MAX}")
        print()

        # Process data
        print("======== 0c. Alice's and Bob's processed data ========")
        a_conv = self.convert(a_data)
        b_conv = self.convert(b_data)
        print(f"Alice: {a_conv}")
        print(f"Bob: {b_conv}")
        print()
        print()

        return (a_data, a_conv), (b_data, b_conv)

    def alice_to_bob(self, a_data):
        """ Alice sending the circuit and its input keys to Bob"""

        print("======== 1a. The circuit that Alice sends to Bob. ========")
        entry = self.circuits[0]
        self.print_tables(entry)

        print("======== 1b. Alice sends her encrypted inputs to Bob. ========")
        circuit, pbits, keys = entry["circuit"], entry["pbits"], entry["keys"]
        garbled_tables = entry["garbled_tables"]
        outputs = circuit["out"]
        a_wires = circuit.get("alice", [])  # Alice's wires
        b_wires = circuit.get("bob", [])  # Bob's wires
        # Map Alice's wires to (key, encr_bit)
        a_inputs = [{w: (keys[w][b], pbits[w] ^ b)
                     for w, b in zip(a_wires, bitset(d, len(a_wires)))} for d in a_data]

        for a_in in a_inputs:
            print({w: a_in[w][1] for w in a_in})
        print()
        print()

        return {"circuit": circuit, "tables": garbled_tables, "pbits": pbits, "keys": keys,
                "b_wires": b_wires, "outputs": outputs}, a_inputs

    def alice_bob_ot(self, circuit, b_data) -> list:
        """ The oblivious transfer occourring between Alice and Bob"""

        print(f"======== 2. OT between Alice and Bob ========")
        keys = circuit["keys"]
        pbits = circuit["pbits"]
        b_wires = circuit["b_wires"]

        if self.ENABLE_OT:
            # Alice maps Bob's wires to a pair (key, encr_bit) for each element in the set.
            b_keys = {w: self._get_encr_bits(pbits[w], key0, key1)
                      for w, (key0, key1) in keys.items() if w in b_wires}

            # Bob creates an array mapping each wire to their bit (in clear).
            b_inputs_clear = [{b_wires[i]: bitset(d, len(b_wires))[i]
                               for i in range(len(b_wires))} for d in b_data]

            # For each element in his set, for each gate, Bob starts an OT to get his input keys.
            b_inputs = [{w: pickle.loads(self.local_ot.start_ot(w, b_keys, b_input))
                         for w, b_input in b_clear.items()} for b_clear in b_inputs_clear]
        else:  # Map Bob's wires to (key, encr_bit)
            print("OT is disabled")
            b_inputs = [{w: (keys[w][b], pbits[w] ^ b)
                         for w, b in zip(b_wires, bitset(d, len(b_wires)))} for d in b_data]
        print()
        print()

        return b_inputs

    def bob_mpc_compute(self, circuit, a_inputs, b_inputs, b_data) -> list:
        """ Bob evaluates the circuit"""
        print("======== 3. The message that Bob sends to Alice ========")
        algo = self.slow_algo if self.ALG_SLOW else self.fast_algo

        print(f"--- MPC_{'slow' if self.ALG_SLOW else 'fast'} ---")
        start = chrono()
        matches = algo(circuit, a_inputs, b_inputs, b_data)
        if self.IS_FLOAT:
            matches = [self.from_fixed(x) for x in matches]
        elap = (chrono() - start) / 1_000_000
        print(f"Bob: Common elements = {matches}")
        print(f"Execution time: {elap} ms.")
        print()
        print()

        return matches

    def alice_mpc_compute(self):
        pass

    def verify_output(self, a_data, b_data, match_yao):
        """ Verify that the MPC output is correct"""

        print("======== 4. Verification of the MPC computations on the plain data. ========")

        # Plain execution on original data to check conversion errors
        print(f"--- Plaintext (on original data) ---")
        start = chrono()
        match = list(set(a_data[0]).intersection(set(b_data[0])))
        match.sort()
        elap = (chrono() - start) / 1_000_000
        print(f"Common elements: {match}")
        print(f"Execution time: {elap} ms.\n")

        # Plain execution on converted data to verify results
        print(f"--- Plaintext (on converted data) ---")
        start = chrono()
        match = list(set(a_data[1]).intersection(set(b_data[1])))
        match.sort()
        if self.IS_FLOAT:
            match = [self.from_fixed(x) for x in match]
        elap = (chrono() - start) / 1_000_000
        print(f"Common elements: {match}")
        print(f"Execution time: {elap} ms.\n")

        if set(match) == set(match_yao):
            print("The two solutions are equal, the MPC is correct!")
        else:
            print("The two solutions are not the same, the MPC is not right.")
        print()
        print()

    def load_data(self, file: str) -> list:
        if self.NUMEL <= 0:
            # Load JSON files
            with open(file) as f:
                db = json.load(f)
            data = db["data"]
        elif self.IS_FLOAT:
            data = [round(random() * self.REAL_MAX, self.DEC_DIGITS) for _ in range(self.NUMEL)]
        else:
            data = [randint(0, self.MAX) for _ in range(self.NUMEL)]

        return data

    def generate_equal_circuit(self):
        """Generate equal elements circuit for n-bit inputs"""
        n = self.BITS
        first = 0
        last = n
        a_in = [x for x in range(first, last)]  # Alice's input

        first = last
        last += n
        b_in = [x for x in range(first, last)]  # Bob's input

        first = last
        last += n
        cmp = [x for x in range(first, last)]  # Equality compare -> XOR

        merge = [[]] * n.bit_length()  # Merge to hide -> OR chain
        for i in range(len(merge)):
            first = last
            last += n >> (i + 1)
            merge[i] = [x for x in range(first, last)]

        # Encode the comparison gates
        gates = [{"id": o, "type": "XOR", "in": [i, j]}
                 for i, j, o in zip(a_in, b_in, cmp)]

        # Encode the first merge step
        for i in range(len(merge[0])):
            gates += [{"id": merge[0][i], "type": "OR",
                       "in": [cmp[2 * i], cmp[2 * i + 1]]}]

        # Encode the other merge steps
        for i in range(1, len(merge)):
            for j in range(len(merge[i])):
                gates += [{"id": merge[i][j], "type": "OR",
                           "in": [merge[i - 1][2 * j], merge[i - 1][2 * j + 1]]}]

        data = {
            "name": self.CIRCUIT_FNAME,
            "circuits": [
                {
                    "id": f"{self.CIRCUIT_FNAME}_{n}bit",
                    "alice": a_in,
                    "bob": b_in,
                    "out": [last - 1],
                    "gates": gates,
                }
            ]
        }
        with open(self.CIRCUIT_FILE, 'w') as f:
            json.dump(data, f, separators=(',', ':'))

    def generate_compare_circuit(self):
        """Generate compare circuit for n-bit inputs"""

        n = self.BITS
        first, last = 0, n
        a_in = [x for x in range(first, last)]  # Alice's input
        first, last = last, last + n
        b_in = [x for x in range(first, last)]  # Bob's input
        first, last = last, last + n

        gates = [
            {"id": last, "type": "XNOR", "in": [
                a_in[0], b_in[0]]},     # Real XOR
            {"id": last + 1, "type": "NOT",
                "in": [last]},              # Placebo OR
            # Placebo AND1
            {"id": last + 2, "type": "NOT", "in": [b_in[0]]},
            {"id": last + 3, "type": "NOT",
                "in": [b_in[0]]},           # Placebo NOT
            # Placebo AND2
            {"id": last + 4, "type": "NOT", "in": [b_in[0]]},
            # Placebo XOR_CMP
            {"id": last + 5, "type": "OR", "in": [b_in[0], b_in[0]]},
        ]
        last += 6
        for i in range(1, n):
            gates += [
                {"id": last, "type": "XOR", "in": [a_in[i], b_in[i]]},
                {"id": last + 1, "type": "OR", "in": [last - 5, last]},
                {"id": last + 2, "type": "AND", "in": [last - 5, last - 1]},
                {"id": last + 3, "type": "NOT", "in": [last - 5]},
                {"id": last + 4, "type": "AND", "in": [last + 3, b_in[i]]},
                {"id": last + 5, "type": "XOR", "in": [last + 2, last + 4]}]
            last += 6

        out = [last - 5, last - 1]
        data = {
            "name": self.CIRCUIT_FNAME,
            "circuits": [
                {
                    "id": f"{self.CIRCUIT_FNAME}_{n}bit",
                    "alice": a_in,
                    "bob": b_in,
                    "out": out,
                    "gates": gates,
                }
            ]
        }
        with open(self.CIRCUIT_FILE, 'w') as f:
            json.dump(data, f, separators=(',', ':'))

    def print_tables(self, entry):
        """Print garbled tables."""
        entry["garbled_circuit"].print_garbled_tables()

    def to_fixed(self, x: float) -> int:
        """Converts a float into a fixed point decimal."""
        return round(x * self.DEC_ROUND) & self.MAX

    def from_fixed(self, x: int) -> float:
        """Converts a fixed point decimal to a float."""
        return round(float(x) / self.DEC_ROUND, self.DEC_DIGITS)

    def convert(self, data: list) -> list:
        """Convert to fixed point, Truncate values, remove duplicates and sort"""
        conv = [self.to_fixed(x) for x in data] if self.IS_FLOAT else data
        conv = list(set([x & self.MAX for x in conv]))
        conv.sort()

        return conv

    def slow_algo(self, circuit, a_inputs, b_inputs, b_data) -> list:
        """ Find common elements using a naive algorithm"""

        circ = circuit["circuit"]
        outputs = circuit["outputs"]
        garbled_tables = circuit["tables"]
        pbits = circuit["pbits"]
        pbits_out = {w: pbits[w] for w in outputs}

        return [b for a_in in a_inputs for b_in, b in zip(b_inputs, b_data) if not
                yao.evaluate(circ, garbled_tables, pbits_out, a_in, b_in)[outputs[0]]]

    def fast_algo(self, circuit, a_inputs, b_inputs, b_data) -> list:
        """
        Since elements are sorted and there are no duplicates, we can use a linear time
        algorithm:
        1. Make two iterators i and j over a_inputs and b_inputs
        2a. If a[i] == b[j], we can safely increment both, since there are no duplicates
        2b. Else, if a[i] > b[j], also all other a[k] for i < k < len(a) will be greater, so we can
        skip them, incrementing j.
        2c. Else, if a[i] < b[j], also all other b[k] for j < k < len(b) will be greater, so we can
        skip them, incrementing i.
        """

        circ = circuit["circuit"]
        outputs = circuit["outputs"]
        garbled_tables = circuit["tables"]
        pbits = circuit["pbits"]
        pbits_out = {w: pbits[w] for w in outputs}
        matches = []
        i, j = 0, 0

        while i < len(a_inputs) and j < len(b_inputs):
            res = yao.evaluate(circ, garbled_tables, pbits_out, a_inputs[i], b_inputs[j])
            if not res[outputs[0]]:  # a[i] == b[j]
                matches.append(b_data[j])
                i += 1
                j += 1
            elif not res[outputs[1]]:  # a[i] > b[j]
                j += 1
            else:  # a[i] < b[j]
                i += 1

        return matches

    def _get_encr_bits(self, pbit, key0, key1):
        return ((key0, 0 ^ pbit), (key1, 1 ^ pbit))


def main(
    ot=True,
    loglevel=logging.WARNING,
    bits=0,
    numel=0,
    algo="slow",
    type="int",
):
    logging.getLogger().setLevel(loglevel)
    local = LocalTest(bits, numel, algo, type, ot)
    local.start()


if __name__ == '__main__':
    import argparse

    def init():
        parser = argparse.ArgumentParser(description="Run Yao protocol.")
        parser.add_argument(
            "--no-ot",
            action="store_true",
            help="disable oblivious transfer")

        parser.add_argument(
            "-a",
            "--algo",
            metavar="algo",
            choices=["fast", "slow"],
            default="fast",
            help="the algorithm to use: 'slow' has a fast circuit but slow algorithm; "
            "'fast' has a slower circuit but a faster algorithm (default 'slow')")

        parser.add_argument(
            "-b",
            "--bits",
            metavar="bits",
            choices=["1", "2", "4", "8", "16", "32", "64"],
            default="0",
            help="the maximum size in bits of the input data")

        parser.add_argument(
            "-n",
            "--numel",
            metavar="numel",
            default="0",
            help="the number of random elements: if set to 0, uses alice.json and bob.json as "
            "inputs (default '0')")

        parser.add_argument(
            "-t",
            "--type",
            metavar="type",
            choices=["int", "float"],
            default="int",
            help="The datatype of the random elements (if enabled), either 'int' or 'float' "
            "(default 'int'")

        main(
            ot=not parser.parse_args().no_ot,
            bits=int(parser.parse_args().bits),
            numel=int(parser.parse_args().numel),
            algo=parser.parse_args().algo,
            type=parser.parse_args().type
        )

    init()
