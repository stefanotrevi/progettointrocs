import pickle
import hashlib
import util


class LocalObliviousTransfer:
    """A class to simulate Oblivious Transfer locally

    It works exactly as a normal oblivious transfer but the (main) communications between Alice and Bob are
    printed on the console instead of being sent through the socket.

    """

    def __init__(self):
        pass

    def start_ot(self, w, b_keys, b):
        """ Bob sends gate to Alice """
        self.bob_asks_OT_on_wire(w)

        """ Alice retrieves the key pair for the wire w and sends them to Bob """
        pair = (pickle.dumps(b_keys[w][0]), pickle.dumps(b_keys[w][1]))
        self.alice_sends_key_pair(b_keys[w][0], b_keys[w][1], w)

        """ OT Alice side """
        G = util.PrimeGroup()
        self.alice_sends_prime(G.prime, w)
        c = G.gen_pow(G.rand_int())
        self.alice_sends_c(c, w)

        """ OT Bob side """
        x = G.rand_int()
        x_pow = G.gen_pow(x)
        h = (x_pow, G.mul(c, G.inv(x_pow)))
        self.bob_chooses_bit(b, w)

        """ OT Alice side """
        h0 = h[b]
        h1 = G.mul(c, G.inv(h0))
        k = G.rand_int()
        c1 = G.gen_pow(k)
        e0 = util.xor_bytes(pair[0], self.ot_hash(G.pow(h0, k), len(pair[0])))
        e1 = util.xor_bytes(pair[1], self.ot_hash(G.pow(h1, k), len(pair[1])))

        """ Alice sends the triple (c1, e0, e1) to Bob """
        self.alice_sends_triple(c1, e0, e1, w)

        """ Bob retrieves the input """
        e = (e0, e1)
        ot_hash = self.ot_hash(G.pow(c1, x), len(e[b]))
        mb = util.xor_bytes(e[b], ot_hash)
        return mb

    def bob_asks_OT_on_wire(self, w):
        print(f"Bob: I need wire {w}.")

    def alice_sends_key_pair(self, key0, key1, w):
        print(f"Alice: Choose between {key0[0], key1[0]}.")

    def alice_sends_prime(self, G, w):
        pass

    def alice_sends_c(self, c, w):
        pass

    def bob_chooses_bit(self, b, w):
        print(f"Bob: I want the {'second' if b else 'first'} one.")
        print()

    def alice_sends_triple(self, c1, e0, e1, w):
        pass

    @staticmethod
    def ot_hash(pub_key, msg_length):
        """Hash function for OT keys."""
        key_length = (pub_key.bit_length() + 7) // 8  # key length in bytes
        bytes = pub_key.to_bytes(key_length, byteorder="big")
        return hashlib.shake_256(bytes).digest(msg_length)
